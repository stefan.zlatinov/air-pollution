#!/usr/bin/python3
import time
import argparse
import logging
import serial

def setup_logger(name, log_file):
    formatter = logging.Formatter('%(message)s')
    handler = logging.FileHandler(log_file)
    handler.setFormatter(formatter)
    logger = logging.getLogger(name)
    logger.setLevel(logging.INFO)
    logger.addHandler(handler)
    return logger

def sensor_data(serial_port):
    while True:
        start_byte_1 = serial_port.read()
        if start_byte_1 != b'\xaa':
            continue
        start_byte_2 = serial_port.read()
        if start_byte_2 != b'\xc0':
            continue

        pm_2p5_low_byte = serial_port.read()
        pm_2p5_high_byte = serial_port.read()
        pm_10_low_byte = serial_port.read()
        pm_10_high_byte = serial_port.read()

        id_1 = serial_port.read()
        id_2 = serial_port.read()
        checksum_read = serial_port.read()

        data = [pm_10_high_byte, pm_10_low_byte, pm_2p5_high_byte, pm_2p5_low_byte, id_1, id_2]
        checksum_calc = sum([ord(d) for d in data]) % 256

        tail = serial_port.read()
        
        if tail != b'\xab':
            yield 'Tail error'
        
        if checksum_calc != ord(checksum_read):
            print(checksum_calc, ord(checksum_read))
            yield 'Checksum error'
        
        pm_2p5 = (ord(pm_2p5_high_byte) * 256 + ord(pm_2p5_low_byte)) / 10
        pm_10 = (ord(pm_10_high_byte) * 256 + ord(pm_10_low_byte)) / 10
        
        yield pm_2p5, pm_10


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('port', help='port on which the sensor is connected')
    args = parser.parse_args()
    
    problem_logger = setup_logger('problem_logger', 'problems.log')
    data_logger = setup_logger('data_logger', 'air_pollution_data.csv')

    port = args.port
    serial_port = serial.Serial(port, baudrate=9600, bytesize=serial.EIGHTBITS, parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE)
    try:
        for data in sensor_data(serial_port):
            if data == 'Tail error' or data == 'Checksum error':
                problem_logger.info('{}, {}'.format(int(time.time()), data))
            else:
                pm_2p5, pm_10 = data
                data_logger.info('{}, {}, {}'.format(int(time.time()), pm_2p5, pm_10))
            time.sleep(1)
    except KeyboardInterrupt:
        serial_port.close()
